from importer import UserImporter, BookImporter, BookAuthorImporter, ShelfImporter, BookShelfImporter, AuthorImporter

from models import database, User, Book, Author, Shelf, BookShelf, BookAuthor, BookTranslator, UserAuthorRelation,\
    UserRelation


def print_hi(name):
    print(f'Hi, {name}')


def load_data():
    importer_classes = [
        UserImporter, BookImporter, AuthorImporter,
        BookAuthorImporter, ShelfImporter, BookShelfImporter
    ]
    for _class in importer_classes:
        print(_class.load())


def create_tables():
    database.create_tables(
        [
            User, Book, Author, Shelf, BookShelf,
            BookAuthor, BookTranslator, UserAuthorRelation,
            UserRelation
        ]
    )


if __name__ == '__main__':
    print_hi('PyCharm')
    # create_tables()
    load_data()

